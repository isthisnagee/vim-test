if !exists('g:test#javascript#ava#file_pattern')
  let g:test#javascript#ava#file_pattern = '\vtests?/.*\.(js|jsx)$'
endif
